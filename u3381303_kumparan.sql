-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 12, 2018 at 04:04 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u3381303_kumparan`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2018_02_08_025547_create_authors_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news`
--

CREATE TABLE `tbl_news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `topic_id_tmp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_news`
--

INSERT INTO `tbl_news` (`id`, `title`, `slug`, `description`, `topic_id_tmp`, `publish`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sudah Kerja Keras, Shaw Bakal Dapat Kontrak Baru dari MU', 'sudah-kerja-keras-shaw-bakal-dapat-kontrak-baru-dari-mu', 'Manchester - Luke Shaw mulai diandalkan Jose Mourinho lagi di sisi kiri pertahanan Manchester United. Bek kiri ini bahkan dijanjikan kontrak baru.\r\n\r\nShaw sempat lama ditepikan Mourinho dari skuat. Kombinasi dari cedera dan masalah sikap membuat Mourinho tak berkenan memasukkannya ke dalam skuat utama.\r\n\r\nMusim ini, dia melewatkan 16 pekan pertama di Premier League tanpa sekalipun menginjak lapangan. Bahkan cuma dua kali Shaw berada di bangku cadangan.\r\n\r\nNamun sejak Desember, Shaw mulai menarik perhatian Mourinho lagi. Bek 22 tahun itu untuk kali pertama tampil di liga saat MU menang 1-0 atas Bournemouth dan bermain 83 menit.\r\n\r\nKemudian Mourinho mulai lebih sering menurunkannya. Dalam tujuh pertandingan terakhir \'Setan Merah di liga, Shaw tampil penuh di lima kesempatan. Dia juga bermain penuh di dua pertandingan Piala FA pada Januari lalu.\r\n\r\nMourinho mengaku terkesan dengan kerja keras dan progres yang ditunjukkan Shaw. Kontraknya yang akan habis di Juni mendatang pun bakal diperpanjang.\r\n\r\n\"Terkait Luke, itu bukan soal mengubah pikiran saya, cuma soal ada evolusi atau tidak dari potensinya. Cuma soal itu. Karena saya tahu Shaw sejak dia tiba di Premier League dengan Southampton dan saya tahu potensinya, kualitasnya,\" kata Mourinho dikutip Soccerway.\r\n\r\n\"Bisakah dia mengikuti arah saya dan cara saya melihat dan berpikir di sepakbola, cara saya menyukai para pemain di lapangan dan di latihan? Dia membuat upaya besar. Kini, untuk beberapa bulan dia bebas dari cedera minor dan saya sangat senang.\"\r\n\r\n\"Jadi saya rasa konsekuensi alaminya adalah dia akan mendapatkan kontrak barunya dan akan menjadi pemain Manchester United untuk bertahun-tahun ke depan,\" tambahnya.\r\n(raw/mfi)', 'a:3:{i:0;s:2:\"13\";i:1;s:1:\"7\";i:2;s:1:\"3\";}', 1, '2018-02-09 19:21:43', '2018-02-09 19:22:33', NULL),
(2, 'Gattuso Tak Suka Milan Dipuji', 'gattuso-tak-suka-milan-dipuji', 'Milan - Pelatih AC Milan Gennaro Gattuso mengaku tak suka mendengar pujian untuk timnya. Menurut Gattuso, posisi Milan di klasemen membuat timnya itu tak layak untuk dipuji.\r\n\r\nMilan-nya Gattuso mulai memperlihatkan peningkatan. Rossoneri tak terkalahkan dalam tujuh pertandingan terakhirnya di semua kompetisi dan menang empat kali.\r\n\r\nMembaiknya Milan membuat mereka sempat dipuji, salah satunya oleh legenda klub Paolo Maldini. Maldini menyebut kinerja Gattuso mulai membuahkan hasil.', 'a:3:{i:0;s:2:\"12\";i:1;s:1:\"9\";i:2;s:1:\"8\";}', 2, '2018-02-09 19:58:57', '2018-02-09 20:00:57', NULL),
(3, '\'Alexis Sanchez Akan Bantu Lukaku Tambah Produktif\'', 'alexis-sanchez-akan-bantu-lukaku-tambah-produktif', 'London - Pemain Manchester United, Romelu Lukaku, masih belum rajin membobol gawang lawan. Kedatangan Alexis Sanchez dinilai akan membantu Lukaku tambah produktif lagi.\r\n\r\nLukaku memang sudah membukukan sebanyak 12 gol untuk The Red Devils di Liga Inggris musim ini. Dengan nilai transfernya yang mencapai 75 juta pound sterling, jelas jumlah itu masih kurang.\r\n\r\nLukaku beberapa kali mengalami momen puasa gol, utamanya saat MU berhadapan dengan tim enam besar. Di musim ini, dia baru sekali absen membela MU di Liga Inggris.', 'a:4:{i:0;s:1:\"9\";i:1;s:1:\"7\";i:2;s:1:\"2\";i:3;s:1:\"1\";}', 1, '2018-02-09 19:59:27', '2018-02-09 19:59:27', NULL),
(4, 'Peringatan Dortmund ke Pemainnya: Jangan Tiru Dembele dan Aubameyang!', 'peringatan-dortmund-ke-pemainnya-jangan-tiru-dembele-dan-aubameyang', 'Dortmund - Borussia Dortmund memberi peringatan keras kepada para pemain mereka. Bagi yang coba-coba meniru Ousmane Dembele dan Pierre-Emerick Aubameyang akan mendapatkan hukuman.\r\n\r\nDortmund melepas Dembele dan Aubameyang dalam dua bursa transfer terakhir. Dembele dijual ke Barcelona seharga 105 juta euro di musim panas lalu, sementara Aubameyang baru-baru ini pindah ke Arsenal dengan nilai transfer 63,75 juta euro.\r\n\r\nAkan tetapi, kepergian Dembele dan Aubameyang dari Dortmund menyisakan cerita tidak enak. Dua pemain itu lebih dulu bikin ulah agar keinginannya untuk pindah dikabulkan oleh pihak klub.\r\n\r\nDembele sempat mangkir dari latihan Dortmund menjelang kepindahannya ke Barca. Sementara itu, Aubameyang absen dari pertemuan tim sehari menjelang pertandingan, yang membuatnya diparkir.', 'a:4:{i:0;s:2:\"11\";i:1;s:1:\"5\";i:2;s:1:\"4\";i:3;s:1:\"2\";}', 2, '2018-02-09 20:00:47', '2018-02-12 03:00:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_news_topic`
--

CREATE TABLE `tbl_news_topic` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `news_id` tinyint(3) UNSIGNED NOT NULL,
  `topic_id` tinyint(3) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_news_topic`
--

INSERT INTO `tbl_news_topic` (`id`, `news_id`, `topic_id`, `created_at`, `updated_at`) VALUES
(1, 1, 13, NULL, NULL),
(2, 1, 7, NULL, NULL),
(3, 1, 3, NULL, NULL),
(7, 3, 9, NULL, NULL),
(8, 3, 7, NULL, NULL),
(9, 3, 2, NULL, NULL),
(10, 3, 1, NULL, NULL),
(21, 2, 12, NULL, NULL),
(22, 2, 9, NULL, NULL),
(23, 2, 8, NULL, NULL),
(32, 4, 11, NULL, NULL),
(33, 4, 5, NULL, NULL),
(34, 4, 4, NULL, NULL),
(35, 4, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_topic`
--

CREATE TABLE `tbl_topic` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_topic`
--

INSERT INTO `tbl_topic` (`id`, `title`, `slug`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'News', 'news', '2018-02-09 19:13:09', '2018-02-09 19:13:09', NULL),
(2, 'Finance', 'finance', '2018-02-09 19:13:16', '2018-02-09 19:13:16', NULL),
(3, 'Sport', 'sport', '2018-02-09 19:13:32', '2018-02-09 19:13:32', NULL),
(4, 'Otomotif', 'otomotif', '2018-02-09 19:13:39', '2018-02-09 19:13:39', NULL),
(5, 'Food', 'food', '2018-02-09 19:13:46', '2018-02-09 19:13:46', NULL),
(6, 'Health', 'health', '2018-02-09 19:13:51', '2018-02-09 19:13:51', NULL),
(7, 'Sepakbola', 'sepakbola', '2018-02-09 19:14:19', '2018-02-09 19:14:19', NULL),
(8, 'Bulutangkis', 'bulutangkis', '2018-02-09 19:14:28', '2018-02-09 19:14:28', NULL),
(9, 'Politic', 'politic', '2018-02-09 19:14:35', '2018-02-09 19:14:35', NULL),
(10, 'Regional', 'regional', '2018-02-09 19:14:41', '2018-02-09 19:14:41', NULL),
(11, 'Nasional', 'nasional', '2018-02-09 19:14:45', '2018-02-09 19:14:45', NULL),
(12, 'International', 'international', '2018-02-09 19:14:52', '2018-02-09 19:14:52', NULL),
(13, 'Mancanegara', 'mancanegara', '2018-02-09 19:14:58', '2018-02-09 19:14:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$zy/CixpK.CvgZwcGxmxoe.sddUfqNbbvClZymnuUUBRmi7ctV6klW', 'xdH9F0EgqajyKgWMBGHGnZdDQS1EFPTucoMkNBiCf9wMM1Niy752nLVLbhVE', '2018-02-07 19:44:33', '2018-02-07 19:44:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_news_topic`
--
ALTER TABLE `tbl_news_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_topic`
--
ALTER TABLE `tbl_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_news_topic`
--
ALTER TABLE `tbl_news_topic`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tbl_topic`
--
ALTER TABLE `tbl_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
