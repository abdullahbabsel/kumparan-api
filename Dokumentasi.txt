- run "python application.py"
- jalankan dengan Postman (localhost:5000)

filter dengan:
	- title (character bebas | title)
	- status (angka 1-3 | 1:draft,2:publish,3:deleted)
	- topic (angka pada tbl_topic (id) | topic_id_tmp)

example:
	- localhost:5000?title=ta&status=2&topic=3
	- localhost:5000?status=2&topic=3
	- localhost:5000?title=ta&status=2
	- localhost:5000?title=ta
	- localhost:5000?title=milan
	- localhost:5000?title=pelatih
	- localhost:5000?status=1
	- localhost:5000?status=3