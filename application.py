from flask import Flask, request, jsonify
# from flask_sqlalchemy import SQLAlchemy
# from flask_marshmallow import Marshmallow
from flaskext.mysql import MySQL
import os
import json
# import pymysql
import config
import datetime

app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_HOST'] = config.dbHost
app.config['MYSQL_DATABASE_USER'] = config.dbUser
app.config['MYSQL_DATABASE_PASSWORD'] = config.dbPassword
app.config['MYSQL_DATABASE_DB'] = config.dbName

mysql.init_app(app)

# endpoint to show all users
@app.route("/", methods=["GET"])
def get_user():
    conn = mysql.connect()
    cursor = conn.cursor()

    # check params title is_empty
    if request.args.get('title') is not None:
        condTitle = request.args.get('title')
    # check params status is_empty
    if request.args.get('status') is not None:
        condStatus = request.args.get('status')
    # check params topic is_empty
    if request.args.get('topic') is not None:
        condTopic = request.args.get('topic')

    # check empty params
    if request.args.get('title') is not None and request.args.get('status') is not None and request.args.get('topic') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s AND publish = %s AND topic_id_tmp LIKE %s', ("%" + condTitle + "%", condStatus, "%" + condTopic + "%") )
    elif request.args.get('title') is not None and request.args.get('status') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s AND publish = %s', ("%" + condTitle + "%", condStatus) )
    elif request.args.get('title') is not None and request.args.get('topic') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s AND topic_id_tmp LIKE %s', ("%" + condTitle + "%", "%" + condTopic + "%") )
    elif request.args.get('status') is not None and request.args.get('topic') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE AND publish = %s AND topic_id_tmp LIKE %s', (condStatus, "%" + condTopic + "%") )
    elif request.args.get('status') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE AND publish = %s', (condStatus) )
    elif request.args.get('topic') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE topic_id_tmp LIKE %s', ("%" + condTopic + "%") )
    elif request.args.get('title') is not None:
        cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s', ("%" + condTitle + "%") )
    else:
        cursor.execute('SELECT * FROM tbl_news')

    data = [
        dict((cursor.description[i][0], value)
        for i, value in enumerate(row)) for row in cursor.fetchall()
    ]

    return jsonify({ 'data' : data })


    # belum ketemu cara multi params, sehingga dihide dulu, pake cara cepat di atas.
    # conditions = conditions2 = ''

    # check param is_empty
    # if request.args.get('title') is not None or request.args.get('status') is not None or request.args.get('topic') is not None:
    #     conditions = 'WHERE '
        # conditions2 = '('


    # check params title is_empty
    # if request.args.get('title') is not None:
    #     conditions = conditions + 'title LIKE %s '
    #     conditions2 = conditions2 + "'%"+ request.args.get('title') +"%'" + ""
    #     condTitle = request.args.get('title')

    # check params status is_empty
    # if request.args.get('status') is not None:
    #     if request.args.get('title') is not None or request.args.get('topic') is not None:
    #         conditions = conditions + 'AND '

    #     conditions = conditions + 'publish = %s '
    #     conditions2 = conditions2 + request.args.get('status') +", "

    # check params topic is_empty
    # if request.args.get('topic') is not None:
    #     if request.args.get('status') is not None or request.args.get('topic') is not None:
    #         conditions = conditions + 'AND '

    #     conditions = conditions + 'topic LIKE %s '
    #     conditions2 = conditions2 + "'%"+ request.args.get('topic') +"%'" + ", "


    # if request.args.get('title') is not None:
    #     cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s', ("%" + condTitle + "%") )
    # else:
    #     cursor.execute("SELECT * FROM tbl_news")


    # check params is_empty and replace quatation conditions2
    # if request.args.get('title') is not None or request.args.get('status') is not None or request.args.get('topic') is not None:
        # conditions2 = conditions2 + ')'
        # conditions2 = conditions2.replace('"', '')

    # return conditions2
    # return jsonify({ 'conditions' : conditions, 'conditions2' : conditions2, 'test' : "%" + '123' + "% ," })

    # check conditions2 is_empty
    # if conditions2 != '':
    #     cursor.execute("SELECT * FROM tbl_news " + conditions, (conditions2.replace('"', '')))
    # else:
    #     cursor.execute("SELECT * FROM tbl_news")


    # return conditions2
    # return repr(conditions2).replace('"', '')

    # return jsonify({ 'conditions' : conditions, 'conditions2' : conditions2 })

    # condTitle = request.args.get('title')
    # cursor.execute('SELECT * FROM tbl_news WHERE title LIKE %s', ("%" + condTitle + "%") )

    # data = [
    #     dict((cursor.description[i][0], value)
    #     for i, value in enumerate(row)) for row in cursor.fetchall()
    # ]

    # return jsonify({ 'data' : data })


# endpoint to get user detail by id
@app.route("/user/<id>", methods=["GET"])
def user_detail(id):
    conn = mysql.connect()
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM tbl_news WHERE id = %s', (id) )

    data = [
        dict((cursor.description[i][0], value)
        for i, value in enumerate(row)) for row in cursor.fetchall()
    ]

    return jsonify({ 'data' : data })


# endpoint to create new user
@app.route("/user", methods=["POST"])
def add_user():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()

        title = request.form['title']
        slug = request.form['slug']
        description = request.form['description']
        topic_id_tmp = request.form['topic_id_tmp']
        publish = request.form['publish']

        cursor.execute("INSERT INTO tbl_news (title, slug, description, topic_id_tmp, publish, created_at) VALUES ('" + title + "', '" + slug + "', '" + description + "', '" + topic_id_tmp + "', '" + publish + "', now() )")
        conn.commit()

        return jsonify({ 'status' : 'success' })
    except Exception as e:
        return jsonify({ 'status' : 'failed', 'message' : str(e) })


# endpoint to update user
@app.route("/user/<id>", methods=["PUT"])
def user_update(id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()

        title = request.form['title']
        slug = request.form['slug']
        description = request.form['description']
        topic_id_tmp = request.form['topic_id_tmp']
        publish = request.form['publish']

        cursor.execute('UPDATE tbl_news SET title = %s, slug = %s, description = %s, topic_id_tmp = %s, publish = %s, updated_at = %s WHERE id = %s',(title,slug,description,topic_id_tmp,publish,'now()',id) )
        conn.commit()

        return jsonify({ 'status' : 'success' })
    except Exception as e:
        return jsonify({ 'status' : 'failed', 'message' : str(e) })


# endpoint to delete user
@app.route("/user/<id>", methods=["DELETE"])
def user_delete(id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.execute('DELETE FROM tbl_news WHERE id = %s',id)
        conn.commit()

        return jsonify({ 'status' : 'success' })
    except Exception as e:
        return jsonify({ 'status' : 'failed', 'message' : str(e) })


if __name__ == '__main__':
    app.run(debug=True)